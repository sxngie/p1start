package edu.uprm.cse.datastructures.cardealer.model;

import edu.uprm.cse.datastructures.cardealer.util.CarComparator;
import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;

public class CarList {
	static CarComparator comp = new CarComparator();
	
	// Resets and returns the List, so that the API tests can run.
	
	public static final CircularSortedDoublyLinkedList<Car> listOfCars = new CircularSortedDoublyLinkedList<Car>(comp);
	
	public static CircularSortedDoublyLinkedList<Car> getInstance() {

		return listOfCars;
	}
	
	public static void resetCars() {

		listOfCars.clear();
	}


}
