package edu.uprm.cse.datastructures.cardealer.util;

import java.util.Iterator;

import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList.Node;

public class iteratorCSDLL<E> implements Iterator<E>{
	
	/* Iterates through the Circular Sorted Doubly Linked List.  */
	
	Node<E> tempy;
	
	public iteratorCSDLL(Node<E> head) {
		tempy = head;
	}

	@Override
	public boolean hasNext() {
		return (tempy.getNext() != null);
	}

	@Override
	public E next() {
		if (hasNext()) {
			tempy = tempy.getNext();
			return tempy.getElement();
		}
		else {
			return null;
		}
	
	}
 }
