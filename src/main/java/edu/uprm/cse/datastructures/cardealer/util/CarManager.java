package edu.uprm.cse.datastructures.cardealer.util;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.model.CarList;

@Path("/cars")
public class CarManager {

	private static final CircularSortedDoublyLinkedList<Car> carDealer = CarList.listOfCars;
		
		/* 
		 * Returns an array with the contents of the List.
		 */
	
		@GET
	@Produces(MediaType.APPLICATION_JSON)
		public Car[] getCarList() {
			if (carDealer.isEmpty()) {
				return null;
			}
			Car[] carArray = new Car[carDealer.size()];

			for (int i = 0; i < carDealer.size(); i++) {
				carArray[i] = carDealer.get(i);
			}
			
		return carArray;
	}

		/* 
		 * Returns the object with the matching car ID; 
		 *	if the object is not in the List, it throws a NotFoundException.
		 */
		  
		@GET
		@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON) 
		public Car getCar(@PathParam("id") long id) {
			for (int i = 0; i < carDealer.size(); i++) {
				if (carDealer.get(i).getCarId() == id) {
					Response.status(Response.Status.OK).build();

					return carDealer.get(i);
					}
				}
			throw new NotFoundException();
		}
	
		/*
		 *	Adds object to the list.
		 */
		
		@POST
		@Path("/add")
	@Produces(MediaType.APPLICATION_JSON)
		public Response addCar(Car car) {
			for (int i = 0; i < carDealer.size(); i++) {
				if (carDealer.get(i).getCarId() == car.getCarId()) {
					
					return Response.status(409).build();
					}
			}
			
			carDealer.add(car);
			
			return Response.status(201).build();
		}
	
		/* 
		 * Updates a specific object instance in the List, 
		 * by removing the original instance and replacing 
		 * it with the new one. 
		 */
		
		@PUT
		@Path("/{id}/update")
	@Produces(MediaType.APPLICATION_JSON)
		public Response updateCar(Car car) {
			Car tempy = this.getCar(car.getCarId());
			
			if (carDealer.contains(tempy)) {
				carDealer.remove(tempy);
				carDealer.add(car);
				
				return Response.status(Response.Status.OK).build();
			}
			return Response.status(404).build();
		}
		
		/*	
		 *  Deletes an object from the List 
		 *	when identified with matching car ID.
		 */
		
		@DELETE
		@Path("/{id}/delete")
		public Response deleteCar(@PathParam("id") long id) {
			Car tempy = this.getCar(id);
			
			if (carDealer.contains(tempy)) {
				carDealer.remove(tempy);
				
				return Response.status(Response.Status.OK).build();
			}
			return Response.status(404).build();
		}
		
		
		/*
		 *	 Resets the List.
		 */
		
		public void resetCars() {
			
			carDealer.clear();
		}

}
