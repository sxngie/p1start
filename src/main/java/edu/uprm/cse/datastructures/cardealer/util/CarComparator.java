package edu.uprm.cse.datastructures.cardealer.util;

import java.util.Comparator;

import edu.uprm.cse.datastructures.cardealer.model.Car;

public class CarComparator<E> implements Comparator<E> {
	
	/* 
	 * Car Comparator compares two Car instances (Cars: c1 and c2),
	 * by creating a String for their attributes (Brand, Model, Model Option)
	 * and comparing both Strings, using the compareTo() String method.
	 */
	
	@Override
	public int compare(E c1, E c2) {
		Car car1 = (Car) c1;
		Car car2 = (Car) c2;
		
		String c1String = car1.getCarBrand() + car1.getCarModel() + car1.getCarModelOption();
		String c2String = car2.getCarBrand() + car2.getCarModel() + car2.getCarModelOption();
		
		c1String = c1String.toLowerCase();
		c2String = c2String.toLowerCase();
		
		return c1String.compareTo(c2String);
	}

}
