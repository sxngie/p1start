package edu.uprm.cse.datastructures.cardealer.util;

import java.util.Comparator;
import java.util.Iterator;

public class CircularSortedDoublyLinkedList<E> implements SortedList <E> {
	
	private int currentSize;
	private Node<E> header;
	private Comparator<E> comp;
	
	
		@SuppressWarnings({ "unchecked", "rawtypes"})
		public CircularSortedDoublyLinkedList(Comparator<E> comp) {
			
			header = new Node<E>(null, null, null);
			this.header.setNext(header);
			this.header.setPrevious(header);
			this.currentSize = 0;
			this.comp = comp;
	}
	
		public static class Node<E> {
			private E element;
			private Node<E> next;
			private Node<E> previous;
			
			public Node(E element, Node<E> next, Node<E> previous) {
				super();
				this.element = element;
				this.next = next;
				this.previous = previous;
			}
			public Node() {
				super();
			}
			
			public E getElement() {
				return element;
			}
			public void setElement(E element) {
				this.element = element;
			}
			public Node<E> getNext() {
				return next;
			}
			public void setNext(Node<E> next) {
				this.next = next;
			}
			public Node<E> getPrevious() {
				return previous;
			}
			public void setPrevious(Node<E> previous) {
				this.previous = previous;
			}
			
		}
		
		/*
		 * Summons iterator to iterate through List. 
		 */
		
		@Override
		public Iterator<E> iterator() {
			return new iteratorCSDLL<E>(this.header);
		}
		
		/*
		 * Adds the cars in ascending order.
		 */
		
		@Override
		public boolean add(E obj) {
			Node<E> position = this.header;
			Node<E> tempy = new Node<E>(obj, null, null);
			
			if (obj == null) {
				return false;
			}
			
			if (this.isEmpty()) {
				this.header.setNext(tempy);
				tempy.setPrevious(header);
				this.header.setPrevious(tempy);
				tempy.setNext(header);
				
				this.currentSize++;
				return true;
			}
			else {
				while (position.getNext() != header) {
					position = position.getNext();
					if (comp.compare(obj, position.getElement()) <= 0) {
						tempy.setNext(position);
						tempy.setPrevious(position.getPrevious());
						position.getPrevious().setNext(tempy);
						position.setPrevious(tempy);
						
						this.currentSize++;
						return true;
					}
					else if (position.getNext() == header) {
						tempy.setPrevious(header.getPrevious());
						tempy.setNext(header);
						header.getPrevious().setNext(tempy);
						header.setPrevious(tempy);
						
						this.currentSize++;
						return true;
					}
				}
				return false;
			}
		}
		
		/*
		 * Returns the current size of the List.
		 */
		
		@Override
		public int size() {
			return this.currentSize;
		}
		
		/*
		 * Removes given object.
		 */
		
		@Override 
		public boolean remove(E obj) {
			int index = this.firstIndex(obj);
			
			if (index >= 0) {
				return this.remove(index);
			}
			return false;
		}
		
		/*
		 * Removes the object in the given index;
		 * if the index is out of bounds, returns an 
		 * Out of Bounds Exception.
		 */
		
		@Override 
		public boolean remove(int index) {
			if ((index < 0) || (index >= this.currentSize)) {
				throw new IndexOutOfBoundsException();
			}
			else {
				Node<E> tempy = this.header;
				int currentPos = 0;
				Node<E> target = null;
				
				while (currentPos != index) {
					tempy = tempy.getNext();
					currentPos++;
				}
				if (currentPos > this.currentSize) {
					return false;
				}
				else {
					target = tempy.getNext();
					tempy.setNext(target.getNext());
					target.getNext().setPrevious(tempy);
					target.setElement(null);
					target.setNext(null);
					
					this.currentSize--;
					return true;
				}
			}
		}
		
		/*
		 * Removes all instances of an object.
		 */
		
		@Override 
		public int removeAll(E obj) {
			int firstIx = this.firstIndex(obj);
			int lastIx = this.lastIndex(obj);
			int currentSize = this.size();
			
			for (int i = firstIx; i <= lastIx; i++) {
				this.remove(firstIx);
			}
			return currentSize - this.size();
		}
		
		/* 
		 * Returns the first element in the List. 
		 */
		
		@Override 
		public E first() {
			return header.getNext().getElement();
		}
		
		/*
		 * Returns the last element in the List.
		 */
		
		@Override
		public E last() {
			return header.getPrevious().getElement();
		}
		
		
		/*
		 * Returns the object in the given index.
		 */
		
		@Override
		public E get(int index) {
			int i = 0;
			Node<E> tempy = this.header.getNext();
			
			while (i != index) {
				i++;
				tempy = tempy.getNext();
			}
			return tempy.getElement();
		}
		
		/*
		 * Clears all objects from List. 
		 */
		
		@Override 
		public void clear() {
			while (!this.isEmpty()) {
				this.remove(0);
			}
		}
		
		/*
		 * Tests if the List is empty.
		 */
		
		@Override 
		public boolean isEmpty() {
			return (this.currentSize == 0);
		}
		
		/*
		 * Returns the first index of an object.
		 */
		
		@Override
		public int firstIndex(E e) {
			int i = 0;
			
			for (Node<E> tempy = this.header.getNext(); !tempy.equals(this.header); tempy = tempy.getNext(), i++) {
				if (tempy.getElement().equals(e)) {
					return i;
				}
			}
			return -1;
		}
		
		/*
		 * Returns the last index of an object.
		 */
		
		@Override
		public int lastIndex(E e) {
			int i = this.currentSize-1;
			
			for (Node<E> tempy = this.header.getPrevious(); !tempy.equals(this.header); tempy = tempy.getPrevious(), i--) {
				if (tempy.getElement().equals(e)) {
					return i;
				}
			}
			return -1;
		}
	
		/*
		 * Tests if the List contains the
		 * given object. 
		 */
		
		@Override
		public boolean contains(E e) {
			return this.firstIndex(e) >= 0;
		}
	
	}
